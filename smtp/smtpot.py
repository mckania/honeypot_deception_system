import sys
import asyncore
from datetime import datetime
from smtpd import SMTPServer

sys.path.append('../')
from custom_logging.mylogging import CustomLogger

HOST_ADDR = '0.0.0.0'
HOST_PORT = 5025

logger = CustomLogger(app = __file__).logger

class EmlServer(SMTPServer):

    def process_message(self, peer, mailfrom, rcpttos, data, mail_options = None, \
                        rcpt_options = None):
        extrad = {'client_addr': peer}
        logger.info(f'Data received: {data} from: {mailfrom} to: {rcpttos}', \
                    extra = extrad)


def run():
    print('Starting server...')
    server = EmlServer(localaddr = (HOST_ADDR, HOST_PORT), remoteaddr = None)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
	run()