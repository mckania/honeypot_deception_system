# rules for nginx proxy firewall

IP forwarding must be turned on!
```
sudo apt-get update -y
sudo apt-get install dnsutils -y
sudo apt-get install tcpdump -y
sudo apt-get install nginx-full -y 

sudo ip rule add fwmark 1 lookup 100
sudo ip route add local 0.0.0.0/0 dev lo table 100

sudo iptables -t mangle -A PREROUTING -p tcp -s NODE_POOL_RANGE --sport 30022 -j MARK --set-xmark 0x1/0xffffffff
sudo iptables -t mangle -A PREROUTING -p tcp -s NODE_POOL_RANGE --sport 30023 -j MARK --set-xmark 0x1/0xffffffff
sudo iptables -t mangle -A PREROUTING -p udp -s NODE_POOL_RANGE --sport 30053 -j MARK --set-xmark 0x1/0xffffffff
sudo iptables -t mangle -A PREROUTING -p udp -s NODE_POOL_RANGE --sport 30025 -j MARK --set-xmark 0x1/0xffffffff

sudo iptables -A FORWARD -i ens5 -o ens4 -j ACCEPT
sudo iptables -A FORWARD -i ens4 -o ens5 -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -t nat -A POSTROUTING -o ens4 -j MASQUERADE
```
#### Note:

- ens4 - to the Internet
- ens5 - internal network