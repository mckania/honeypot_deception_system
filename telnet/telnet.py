import socket
import threading
import functools
import sys
import traceback
import time

sys.path.append('../')
from custom_logging.mylogging import CustomLogger

#address to listen on
HOST_ADDR = '0.0.0.0'
HOST_PORT = 5023

BUFF_SIZE = 2048

PROMPT = "production:~$ "
WELCOME_MESSAGE = """Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-96-generic x86_64)

* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage

System load:  0.0                Processes:              91
Usage of /:   30.9% of 19.56GB   Users logged in:        2
Memory usage: 3%                 IP address for enp1s4:  192.168.13.124
Swap usage:   0%                 IP address for docker0: 172.17.0.1
22 packages can be updated.
0 updates are security updates.
Last login: Thu Apr 16 18:30:48 2020""".replace("\r\n", "\n").replace("\n", "\r\n")

logger = CustomLogger(app = __file__).logger
print = functools.partial(print, flush=True)

def handler_telnet_connection(client_socket, client_address):
    try:
        handler_telnet_connection_worker(client_socket, client_address)
    except:
        logger.debug('Exception in thread occured... Exiting...')


def handler_telnet_connection_worker(client_socket, client_address):
    """Worker for each connection"""

    stop_event = False
    extrad = {'client_addr': client_address}
    logger.info = functools.partial(logger.info, extra=extrad)
    logger.info(f'Connection from: {extrad["client_addr"]}')

    data_rec = client_socket.recv(BUFF_SIZE)
    if data_rec != '\r'.encode() and data_rec != '\r\n'.encode() and \
        data_rec != ''.encode() and data_rec != ' '.encode():
        try:
            logger.info(f'Data received: {data_rec.decode()}')
        except UnicodeError:
            logger.info(f'Data received: {data_rec}')

    client_socket.sendall(WELCOME_MESSAGE.encode())
    client_socket.sendall('\n'.encode())
    client_socket.sendall(PROMPT.encode())

    while not stop_event:
        data_rec = client_socket.recv(BUFF_SIZE)
        try:
            if data_rec != '\r'.encode() and data_rec != '\r\n'.encode() and \
                data_rec != ''.encode() and data_rec != ' '.encode():
                logger.info(f'Data received: {data_rec.decode()}')
            else:
                pass
            for letter in data_rec.decode():
                if letter == '\r':
                    client_socket.sendall(PROMPT.encode())
                else:
                    pass
        except UnicodeError:
            logger.info(f'Data received (bytes): {data_rec}')
        except:
            stop_event = True


def main():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((HOST_ADDR, HOST_PORT))
        sock.listen(100)
        print("Listening for connection ...")
    except Exception as e:
        print("*** Bind failed: " + str(e))
        traceback.print_exc()
        sys.exit(1)

    while True:
        try:
            client_socket, client_address = sock.accept()
        except Exception as e:
            print("*** Listen/accept failed: " + str(e))
            traceback.print_exc()
            sys.exit(1)
        
        th = threading.Thread(
            target=handler_telnet_connection,
            args=(client_socket, client_address))
        th.daemon = True
        th.start()


if __name__ == "__main__":
    main()
