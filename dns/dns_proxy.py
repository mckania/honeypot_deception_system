  
import sys
from time import sleep
from dnslib import server
from dnslib import *

sys.path.append('./')
sys.path.append('../')
from custom_logging.mylogging_dns import CustomDNSLogger

# Customize the port and address of your local server to suit your needs (e.g. localhost -> 0.0.0.0)
HOST_ADDR = '0.0.0.0'
HOST_PORT = 5053

# Customize the address and port of the external DNS server
EXTERNAL_DNS_ADDR = '8.8.8.8'
EXTERNAL_DNS_PORT = 53
MY_DOMAIN = 'pasieka.agh-uni.com'
MY_NS = 'ns1.agh-uni.com'
MY_DOMAIN_IP = '239.136.254.248'


class SpecialResolver:
    def resolve(self, request, handler):
        d = request.reply()
        query = request.get_q()
        query_name = str(query.qname)
        
        # Custom response for my doamin
        if MY_DOMAIN in query_name:
            # Answers
            d.add_answer(*RR.fromZone(f"{MY_DOMAIN} 136 A {MY_DOMAIN_IP}"))
      
            # Authoritative Name Servers
            d.add_auth(*RR.fromZone(f"{MY_DOMAIN} 128505 NS {MY_NS}"))

        # Recursively query another DNS server for other domains
        else:
            answear = DNSRecord.parse(DNSRecord.question(query_name)
            .send(EXTERNAL_DNS_ADDR, EXTERNAL_DNS_PORT))

            for resourceRecord in answear.rr:
                d.add_answer(resourceRecord)
        return d


if __name__ == "__main__":

    print('Starting DNS proxy...')
    logger = CustomDNSLogger(__file__)
    resolver = SpecialResolver()
    server = server.DNSServer(resolver, address = HOST_ADDR, port = HOST_PORT, logger = logger)
    server.start_thread()

    while True:
        sleep(1)