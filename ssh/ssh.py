import base64
import os
import socket
import sys
import threading
import traceback
import functools
import logging
from binascii import hexlify
import paramiko
from paramiko.py3compat import b, u, decodebytes

sys.path.append('../')
from custom_logging.mylogging import CustomLogger

# address to listen on
HOST_ADDR = '0.0.0.0'
HOST_PORT = 5022

# sample key for auth
host_key = paramiko.RSAKey(filename="rsa.key")

logger = CustomLogger(app = __file__).logger

def handler_ssh_connection(transport, server, client_addr, logger, client):
    try:
        handler_ssh_connection_worker(transport, server, client_addr, logger)
    except:
        logger.debug('Exception in thread occured... Exiting...')


def handler_ssh_connection_worker(transport, server, client_addr, logger):

    stop_event = False
    # wait for auth
    chan = transport.accept(20)

    if chan is None:
        logging.debug('*** No channel.')
        sys.exit(1)

    logger.debug('Authenticated!')
    server.event.wait(10)

    if not server.event.is_set():
        logger.debug('*** Client never asked for a shell.')
        sys.exit(1)

    chan.send(
"""Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-96-generic x86_64)

* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage

System load:  0.0                Processes:              91
Usage of /:   30.9% of 19.56GB   Users logged in:        2
Memory usage: 3%                 IP address for enp1s4:  192.168.13.124
Swap usage:   0%                 IP address for docker0: 172.17.0.1
22 packages can be updated.
0 updates are security updates.
Last login: Thu Apr 16 18:30:48 2020""".replace("\r\n", "\n").replace("\n", "\r\n"))

    f = chan.makefile("rU")

    PROMPT = "production:~$ "
    chan.send('\n\r')
    chan.send(PROMPT)

    line = ""

    while not stop_event:
        try:
            ch = f.read(1)
            try: 
                ch = ch.decode('utf-8')
            except UnicodeDecodeError:
                continue

            line += ch

            if ch == '\r':
                if line != '\r':
                    msg = f'SHELL_LINE: {line}'
                    logger.info(msg)
                line = ""
                chan.send("\r\n")
                chan.send(PROMPT)
            else:
                chan.send(ch)
        except: 
            stop_event = True

    chan.close()


class Server(paramiko.ServerInterface):
    '''Implementation of SSH Honeypot Server.
        It does not provide any functionalities 
        except accepting connection and logging 
        input data do stdout'''

    def __init__(self, client_addr, logger):
        self.event = threading.Event()
        self.extrad = {'client_addr': client_addr}
        self.logger = logger
        self.logger.info = functools.partial(self.logger.info, extra = self.extrad)

    def check_channel_request(self, kind, chanid):
        if kind == "session":
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_password(self, username, password):
        if username and password:
            self.logger.info(f'username: {username}, password: {password}')
            return paramiko.AUTH_SUCCESSFUL
        return paramiko.AUTH_FAILED

    def check_auth_publickey(self, username, key):
        self.logger.info(f'username: {username}, key fingerprint: \
                            {u(hexlify(key.get_fingerprint()))}')
        return paramiko.AUTH_SUCCESSFUL

    def get_allowed_auths(self, username):
        return "password,publickey"

    def check_channel_shell_request(self, channel):
        self.event.set()
        return True

    def check_channel_subsystem_request(self, channel, name):
        return True

    def check_channel_forward_agent_request(self, channel):
        return False

    def check_channel_pty_request(
        self, channel, term, width, height, pixelwidth, pixelheight, modes):
        return True


def main():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((HOST_ADDR, HOST_PORT))
    except Exception as e:
        print(f'*** Bind failed: {str(e)}')
        traceback.print_exc()
        sys.exit(1)

    sock.listen(100)
    print('Listening for connection ...')

    while True:
        try:
            client, addr = sock.accept()
            client_addr = addr
            extrad = {'client_addr': f'{client_addr}'}
            logger.info = functools.partial(logger.info, extra = extrad)
        except Exception as e:
            print(f'*** Listen/accept failed: {str(e)}')
            traceback.print_exc()
            sys.exit(1)

        try:
            transport = paramiko.Transport(client)
            transport.add_server_key(host_key)
        except Exception as e:
            logger.debug(f'Failed in transport: {str(e)}')
            
        try:
            server = Server(client_addr, logger)
            try:
                transport.start_server(server=server)
            except paramiko.SSHException:
                logger.debug('SSH negotation failed')
                sys.exit(1)
        except:
            logger.debug('Error while creating server...')
            sys.exit(1)

        
        th = threading.Thread(
            target=handler_ssh_connection,
            args=(transport, server, client_addr, logger, client))
        th.daemon = True
        th.start()


if __name__ == "__main__":
    main()
