import logging
import sys
import binascii
import time

from dnslib.server import DNSLogger
from dnslib import QTYPE

class CustomDNSLogger(DNSLogger):
    """Logger for logging to stdout for dns.py"""


    def __init__(self, app, log='+recv,+request,-reply', prefix=False):
        super().__init__(log, prefix)
        self.app = app


    def log_recv(self, handler, data):
        t = time.strftime("%Y-%m-%d %H:%M:%S")

        print(f"{t} from: {self.app} "\
        f"src address: ('{handler.client_address[0]}', {handler.client_address[1]}) message: {handler.protocol} {data}")
            
            
    def log_request(self, handler, request):
        t = time.strftime("%Y-%m-%d %H:%M:%S")

        print(f"{t} from: {self.app} "\
        f"src address: ('{handler.client_address[0]}', {handler.client_address[1]}) message: {handler.protocol} "
        f"{request.q.qname}, {QTYPE[request.q.qtype]}")