import dns.resolver

'''Simple scirpt to test manually functionality of the DNS service'''

my_resolver = dns.resolver.Resolver()

my_resolver.nameservers = ['34.91.97.131']

answer1 = my_resolver.resolve('google.com')
answer2 = my_resolver.resolve('pasieka.agh-uni.com')

print(answer1, answer2)