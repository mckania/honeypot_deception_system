import sys
import smtplib

'''Simple scirpt to test manually functionality of the SMTP service'''

def prompt(prompt):
    return input(prompt).strip()

if __name__ == "__main__" and len(sys.argv) == 3:
    fromaddr = prompt("From: ")
    toaddrs  = prompt("To: ").split()
    print("Enter message, end with ^D (Unix) or ^Z (Windows):")

    # Add the From: and To: headers at the start!
    msg = ("From: %s\r\nTo: %s\r\n\r\n"
        % (fromaddr, ", ".join(toaddrs)))

    while True:
        try:
            line = input()
        except EOFError:
            break
        if not line:
            break
        msg = msg + line

    print("Message length is", len(msg))

    server = smtplib.SMTP(host = str(sys.argv[1]), port = str(sys.argv[2]))
    server.set_debuglevel(1)
    server.sendmail(fromaddr, toaddrs, msg)
    server.quit()
    sys.exit(0)