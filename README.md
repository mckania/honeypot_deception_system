# honeypot_deception_system

Implementation and verification of a honeypot system 
<br>
[Bachelor's Thesis](https://gitlab.com/mckania/honeypot_deception_system/-/blob/master/bachelor_thesis.pdf) at AGH University of Science and Technology
<br>

![drawing][architecture]

[architecture]: honeypots.png "architecture"
