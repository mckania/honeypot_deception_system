# gcloud configs
### note:
Following example is using zones europe-west4 and us-east4 for clusters deployment

### VPCs
gcloud compute networks create internet-europe --subnet-mode=custom

gcloud compute networks create internal-europe --subnet-mode=custom

gcloud compute networks create internet-us --subnet-mode=custom

gcloud compute networks create internal-us --subnet-mode=custom

gcloud compute networks subnets create internet-europe \\\
--network=internet-europe --range=10.164.1.0/24 --region=europe-west4

gcloud compute networks subnets create internal-europe \\\
--network=internal-europe --range=10.164.2.0/24 --region=europe-west4

gcloud compute networks subnets create internet-us \\\
--network=internet-us --range=10.150.1.0/24 --region=us-east4

gcloud compute networks subnets create internal-us \\\
--network=internal-us --range=10.150.2.0/24 --region=us-east4

### k8s clusters
gcloud container clusters create honets-agh-europe  \\\
 --cluster-version 1.16.13-gke.401  \\\
 --zone us-east4-a  \\\
 --node-locations europe-west4-a
--network=internal-europe  \\\
 --subnetwork=internal-europe

gcloud container node-pools create pool-europe  \\\
--cluster=honets-agh-europe  \\\
--no-enable-autoupgrade  \\\
--no-enable-autorepair \\\
--num-nodes=1  \\\
--zone=us-europe-west4-a  \\\
--machine-type=e2-highcpu-4  \\\
--tags=honet

gcloud container clusters create honets-agh-us  \\\
 --cluster-version 1.16.13-gke.401  \\\
 --zone us-east4-a  \\\
 --node-locations us-east4-a  \\\
 --network=internal-us  \\\
 --subnetwork=internal-us

gcloud container node-pools create pool-us  \\\
--cluster=honets-agh-us  \\\
--no-enable-autoupgrade  \\\
--no-enable-autorepair \\\
--num-nodes=1  \\\
--zone=us-east4-a  \\\
--machine-type=e2-highcpu-4  \\\
--tags=honet

### VMs
gcloud compute instances create proxy-europe \\\
--zone=us-europe-west4-a --machine-type=g1-small \\\
--network-interface network=internet-us,subnet=internet-us,private-network-ip=10.164.1.2 \\\
--network-interface network=internal-us,subnet=internal-us,private-network-ip=10.164.2.2 \\\
--tags=proxy \\\
--can-ip-forward

gcloud compute instances create proxy-us \\\
--zone=us-east4-a --machine-type=g1-small \\\
--network-interface network=internet-us,subnet=internet-us,private-network-ip=10.150.1.2 \\\
--network-interface network=internal-us,subnet=internal-us,private-network-ip=10.150.2.2 \\\
--tags=proxy \\\
--can-ip-forward

### routes

gcloud compute routes create default-from-honet-us-to-proxy-us \\\
--destination-range=0.0.0.0/0 \\\
--network=internal-us \\\
--next-hop-instance=proxy-us \\\
--next-hop-instance-zone=us-east4-a \\\
--priority=100 \\\
--tags=honet 

gcloud compute routes create dns-from-honet-us-to-google-dns \\\
--destination-range=8.8.8.8/32 \\\
--network=internal-us \\\
--next-hop-gateway=default-internet-gateway \\\
--priority=10 \\\
--tags=honet

gcloud compute routes create from-honet-us-to-k8s-master \\\
--destination-range=35.245.223.133/32 \\\
--network=internal-us \\\
--next-hop-gateway=default-internet-gateway \\\
--priority=10 \\\
--tags=honet

gcloud compute routes create from-honet-us-to-elk \\\
--destination-range=35.228.108.180/32 \\\
--network=internal-us \\\
--next-hop-gateway=default-internet-gateway \\\
--priority=10 \\\
--tags=honet

### firewall rules

gcloud compute firewall-rules create allow-honets-proxy-us \\\
--allow tcp:22,tcp:23,tcp:24,tcp:465,tcp:587,tcp:2525,udp:53 \\\
--target-tags=proxy \\\
--source-ranges=0.0.0.0/0 \\\
--network=internet-us

gcloud compute firewall-rules create allow-ssh-proxy-us \\\
--allow tcp:2022 \\\
--target-tags=proxy \\\
--source-ranges=0.0.0.0/0 \\\
--network=internet-us

gcloud compute firewall-rules create allow-ssh-honet-us \\\
--allow tcp:22 \\\
--target-tags=honet \\\
--source-ranges=0.0.0.0/0 \\\
--network=internal-us

gcloud compute firewall-rules create allow-all-internal-us \\\
--action allow \\\
--rules all \\\
--target-tags=proxy\\\
--source-ranges=0.0.0.0/0 \\\
--network=internal-us

gcloud compute firewall-rules create allow-to-elk \\\
--action allow \\\
--rules tcp:9200 \\\
--target-tags=elk \\\
--source-ranges=34.91.0.0/16

gcloud compute firewall-rules create allow-to-kibana \\\
--action allow \\\
--rules tcp:5601 \\\
--target-tags=elk \\\
--source-ranges=\<your-ip-address>

kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=\<gitlab-username> --docker-password=\<gitlab-password>